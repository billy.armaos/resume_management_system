FROM nginx

COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY /resume_management_system/static /static/static
