FROM python:3

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN python3 -m pip install --upgrade pip

COPY ./requirements.txt .
RUN pip3 install --no-cache-dir -r requirements.txt

RUN mkdir  /source/

WORKDIR /source/

COPY . .

CMD ["gunicorn", "resume_management_system.wsgi", "-b", "0.0.0.0", "-p", "8000"]
