# Resume Management System API

## Introduction
This project is multi-container application written using Docker-Compose.
The API for this project is built using the Django REST framework (DRF).
Gunicorn is used as a webserver while nginx acts as a reverse proxy and static files' server.
PostgreSQL is used as a database and RabbitMQ as a task queue for Celery.
On top of these tools, djangorestframework-simplejwt is used to allow JSON Web Token (JWT) authentication.
Also, openapi/swagger-ui is used to provide the API documentation.

## How To Run
The source code for this project can be found in this repository. 
Feel free to browse around to figure out the specifics of this particular implementation. 

The simplest way to run the API is to download the file `docker-compose.yml` from this repository.
Create a `.env` file in the same directory with `docker-compose.yml` and specify the following variables. 
For convenience some values have been provided with the exception of EMAIL_HOST_USER and EMAIL_HOST_PASSWORD.
```
SECRET_KEY="=@_jjki*kqp5snq_8%ss!o=*y)4zgnqyhsx2nrygp!xbb!^0%t"
RABBITMQ_DEFAULT_USER=user
RABBITMQ_DEFAULT_PASS=password
POSTGRES_DB=resume_management_system_db
POSTGRES_USER=postgres
POSTGRES_PASSWORD=password
DEBUG=False
EMAIL_HOST=smtp.gmail.com
EMAIL_USE_TLS=True
EMAIL_PORT=587
EMAIL_HOST_USER=YOUR_GMAIL_ADDRESS
EMAIL_HOST_PASSWORD=YOUR_GMAIL_PASSWORD
```

After you save the `.env` file, run: 
```bash
docker-compose pull
```
This will pull all the necessary images from docker hub.
Once the pull is finished, you can go ahead and run:
```bash
docker-compose up
```
This command will run the API on port 80 of your system.

## Using the API
There are two main ways to use the API.
Both are described in the subsections "Intended Use" and "Browsable API" bellow.
The "Intended Use" section describes how one should use the API programmatically.
The "Browsable API" section on the other hand has been included to allow ease of access, through a more friendly interface.
If at any point you need more details about the API schema, you can visit `/api/v1/swagger-ui/`.
Keep in mind however, that swagger-ui only displays the methods that the user has access to.
If you want to see the schema of a method that requires authentication you need to log in first.

### Browsable API
The browsable API comes out of the box with the use of DRF.
To enable its use, session authentication needs to be enabled.
This is not intended to be used in production since it is not as safe as the JWT authentication.
It has only been added for the ease of use that it provides to a human user of the API.
To use the browsable API, simply navigate to localhost with your browser.
This takes you to the API root.
From there you can click on the links for `users` or `resumes` to navigate to the corresponding endpoints.
Assuming this is your first visit at the API, you will need to create a user. 
This can be done by navigating to `/api/v1/users/` and then sending a POST request using the HTML form that is provided at the bottom of the page.
Once you have created the user, you can log in using the credentials of the account you created.
To do this, just navigate to `/api-auth/login/` or use the handy "Log in" button at the top right of the page.
After logging in, you can navigate to `/api/v1/resumes/` to upload a resume.
Once a resume is uploaded (and assuming that the email related environment variables are set up correctly), you will receive an email stating the size of your resume.

### Intended Use
To create an account without using the browsable API just use the following command:
```bash
curl -X POST -H 'Content-Type: application/json' -d '{"username": "USERNAME", "password": "PASSWORD", "password2": "PASSWORD"}' http://127.0.0.1/api/v1/users/
```
To get the authentication token for this user you can use the command:
```bash
curl -X POST -H 'Content-Type: application/json' -d '{"username": "USERNAME", "password": "PASSWORD"}' http://127.0.0.1/api/v1/token/
```
The response we get looks like the following:
```json
{
    "refresh": "REFRESH_TOKEN",
    "access": "ACCESS_TOKEN"
}
```
You can now make authenticated requests using the access_token by setting a header of the form:
```
"Authorization: Bearer ACCESS_TOKEN"
```
As a final note, keep in mind that the access token is short-lived.
To obtain a new one once expired you can use the refresh token like so:
```bash
curl -X POST -H 'Content-Type: application/json' -d '{"refresh":"REFRESH_TOKEN"}' http://127.0.0.1/api/v1/token/refresh/
```

## Viewing the docs
For a better user experience, the API includes its documentation, which is produced using Swagger/OpenAPI. 
The docs are accessible through a browser at `http://127.0.0.1/api/v1/swagger-ui/`.

Keep in mind however, that swagger-ui only displays the methods that the user has access to.
If you want to see the schema of a method that requires authentication you need to log in first.
You can do this by navigating to `http://127.0.0.1/api/v1/api-auth/login/?next=/api/v1/swagger-ui/`

This requires session authentication which is less secure than JWT authentication, but it has still been enabled for better ease of use.

## Current Flaws
This piece of software is not ready for production. 
There are several things about it that still need to be changed or improved.
The most important ones are outlined here.

### SSL
This project does not use SSL.
This would have to change before the software reaches production.

### Session Authentication
As mentioned above, this project uses session authentication because it allows the use of the browsable API
This is not at all necessary for the API to work, therefore it is not considered a production feature.
On the contrary, it is more of a development feature, so that one can more easily inspect the structure of the API.
This needs to be removed for production (which is as simple as commenting out a single line of code).
