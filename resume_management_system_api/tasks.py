from django.core.mail import send_mail

from resume_management_system.celery_app import app


@app.task(bind=True, default_retry_delay=10, max_retries=3, name='process_resume')
def process_resume(self, email_to, message_body, **kwargs):
    send_mail(
        subject='Processed Resume',
        message=message_body,
        from_email=None,
        recipient_list=[email_to],
    )
    return f'Email sent to {email_to}.'
