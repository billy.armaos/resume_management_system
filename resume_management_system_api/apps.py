from django.apps import AppConfig


class ResumeManagementSystemApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'resume_management_system_api'
