from django.contrib.auth.models import User
from django.http import HttpResponseForbidden, FileResponse
from django.shortcuts import get_object_or_404
from django.views import View
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from resume_management_system import settings
from resume_management_system.celery_app import app
from .models import Resume
from .permissions import UserSpecificPermission
from .serializers import UserSerializer, ResumeSerializer


class UserViewSet(ModelViewSet):
    queryset = User.objects.none()
    serializer_class = UserSerializer
    permission_classes = [UserSpecificPermission]

    def get_queryset(self):
        if self.request.user.is_staff:
            return User.objects.all()
        else:
            return User.objects.filter(id=self.request.user.id).all()


class ResumeViewSet(ModelViewSet):
    queryset = Resume.objects.none()
    serializer_class = ResumeSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        if self.request.user.is_staff:
            return Resume.objects.all()
        else:
            return Resume.objects.filter(owner=self.request.user).all()

    def send_email(self, file_size):
        app.send_task(
            name='process_resume',
            kwargs={
                'email_to': self.request.user.email,
                'message_body': f'Your resume size is {round(file_size / 1024.0, 1)} kb.'
            }
        )

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
        self.send_email(file_size=serializer.validated_data["file"].size)

    def perform_update(self, serializer):
        super().perform_update(serializer)
        self.send_email(file_size=serializer.validated_data["file"].size)


class DocumentDownload(View):
    def get(self, request, relative_path):
        document = get_object_or_404(Resume, file=relative_path)
        if not request.user.is_superuser and document.owner != request.user:
            return HttpResponseForbidden()
        absolute_path = '{}/{}'.format(settings.MEDIA_ROOT, relative_path)
        response = FileResponse(open(absolute_path, 'rb'), as_attachment=True)
        return response
