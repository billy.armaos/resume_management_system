from django.db import models

from resume_management_system_api.validators import FileValidator


class Resume(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)
    file = models.FileField(validators=[FileValidator(max_size=1024 * 1024 * 10, content_types=('application/pdf',))])
    owner = models.OneToOneField('auth.User', on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return f'Resume created at {self.created_at} and updated at {self.updated_at}.'

    class Meta:
        ordering = ['-created_at']
