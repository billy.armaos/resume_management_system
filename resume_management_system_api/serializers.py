from django.contrib.auth import password_validation
from django.contrib.auth.models import User
from django.core import exceptions
from rest_framework import serializers

from .models import Resume


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(write_only=True)
    password2 = serializers.CharField(label='Repeat Password', write_only=True)

    def validate(self, data):
        # get the password from the data
        password1 = data.get('password')
        password2 = data.pop('password2')

        if password1 != password2:
            raise serializers.ValidationError({'password2': "Passwords don\'t match."})

        user = User(data)

        errors = dict()
        try:
            # validate the password and catch the exception
            password_validation.validate_password(password=password1, user=user)

        # the exception raised here is different from serializers.ValidationError
        except exceptions.ValidationError as e:
            errors['password'] = list(e.messages)

        if errors:
            raise serializers.ValidationError(errors)

        return super(UserSerializer, self).validate(data)

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
            email=validated_data['email']
        )
        return user

    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'password', 'password2', 'is_staff', 'resume']
        read_only_fields = ['is_staff', 'resume']


class ResumeSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedRelatedField(view_name='user-detail', read_only=True)
    class Meta:
        model = Resume
        fields = ['url', 'created_at', 'updated_at', 'owner', 'file']

